<?php

class IndexController extends Controller {

    function __construct() {
        parent::__construct();
    }

    public function defaultAction() {

        try {
            
            $view = new DocumentView( 'home', 'Home' );
            
        } catch (Exception $exc) {
            
            echo $exc->getMessage();
            prd( $exc->getTraceAsString() );
        }
        
        echo $view;
    }

}