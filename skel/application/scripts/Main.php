<?php

import('cli.CLIController');

class MainScript extends CLIController {

    public function defaultAction () {
        $this->writeLn("Test cli command, try:");
        $this->writeLn("--task version");
        $this->writeLn("--task --debug giveme_an_error");

        return 0;
    }

    public function versionAction() {

        $app = $this->config->get('app.name');
        $version = $this->config->get('app.version');

        $this->writeLn("{$app} version: {$version}");

        return 0;
    }

    public function givemeAnErrorAction () {
        throw new Exception ("This is a sample error");
    } 

}
