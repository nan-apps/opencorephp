<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

// namespace util;

/**
 * TODO finish
 * @author ZedPlan Team (opencorephp@zedplan.com)
 * @package util
 */
class Arrays {
	
	private function __construct() { }

	/**
	 * Build array with values extracted from $haystack
	 * @param array[] $haystack
	 * @param string $key key to look for
	 * @return array
	 */
	static function collect(array $haystack, $key)
	{
		$data = array();
		if (is_array($key)) {
			foreach ($haystack as $row) {
				$_v = array();
				foreach($key as $k) {
					$_v[] = isset($row[$k]) ? $row[$k] : null;
				}
				$data[] = $_v;	
			}
		}
		else {
			foreach ($haystack as $row) {
				$data[] = isset($row[$key]) ? $row[$key] : null;	
			}
		}
		return $data;
	}
	
	/**
	 * Combine matrix columns and create an array using the values of $key1 as keys and $keys2 as values.
	 * 
	 * @param mixed[][] $matrix
	 * @param string $key1
	 * @param string $key2
	 * @return mixed[]
	 * @throws InvalidArgumentException if any key is invalid or $haystack is not a matrix
	 */
	static function combineColumns(array $matrix, $key1, $key2)
	{
		$data = array();
		foreach ($matrix as $row) {
			if (!is_array($row)) {
				throw new InvalidArgumentException("'$row' is not an array.");
			}
			if (!array_key_exists($key1, $row)) {
				throw new InvalidArgumentException("Key '$key1' does not exist in haystack.");
			}
			if (!array_key_exists($key2, $row)) {
				throw new InvalidArgumentException("Key '$key2' does not exist in haystack.");
			}
			$data["$key1"] = $row[$key2];
		}
		return $data;
	}
}

?>