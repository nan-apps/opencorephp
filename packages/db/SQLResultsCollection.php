<?php

/**
 * Description of SQLResultsCollection
 *
 * @author guille
 * @ version 0.3 beta
 * 
 * @changelog
 * 0.3 Se agrega metodo rewind. Se mejora variables del paginador.
 */
class SQLResultsCollection {
    
    private $mainField = null; // PK name, used in bulk actions
    private $items = array();
    private $fetchedItem = 0;
    private $paginationOptions;
            
    function __construct(){
        
        try {
                        
            $args = func_get_args();
            if (!empty($args) && !empty($args[0])) {
                $this->setMainField($args[0]); // this should be a primary key 
            }
            
        } catch( Exception $exc ){
            fb($exc);
        }
    }
    
    function add($item){
        
        array_push($this->items, $item);
    }
  
    function first(){
       
        return ireturn($this->items[0]);
    }
    
    function last(){
        $items = $this->items;
        return ireturn($items[ count($items)-1 ]);
    }
    
    function count(){
     
        return count( $this->items );
    }
    
//    function isOdd(){ // NOTE: this method must belong to single object, not to Collection
//        $attrs = (array) $this;
//        $numItems = count($attrs);
//        return ($numItems)
//    }
    
    function fetch($index = null){
        
        if (empty( $index )) {
            
             $index = $this->fetchedItem;
             $this->fetchedItem++;
        }
       
        //return ireturn($this->items[$index]);        
        /* el ireturn al recibir parametro por referencia, cuando item no existe genera una nueva posicion en el array en null
         * agregando un item al resultset, pero en null, esto genera errores si se quiere usar el resultset luego de recorrerlo una vez
         */
        if( isset( $this->items[$index] ) )
            return $this->items[$index];        
        return false;
    }
    
    function reset(){
       
        $this->fetchedItem = 0;      
    }
    
    /* Alias for reset() */
    function rewind(){
       
        $this->reset();      
    }
    
    function isEmpty(){
        
        return empty($this->items);
        
    }
    
    function fetchAll(){
        
        return ireturn($this->items);
    }
    
    function toArray(){
        
        $array = array();
        
        if($this->isEmpty())
            return  $array;
            
        foreach ($this->items as $item) {
            $array[] = $item->getData();
        }
            
        return $array;
        
    }
    
   /*
    * Used internally by the system. Called by BaseSQLModel
    * 
    *   @param $rs ResultSet
    * 
    *   @param $paginationOptions  array(
    *                   'resultsPerPage' => $resultsPerPage,
    *                           'order' => $order,
    *                           'uriVar' => $uriVar,   //The name of the var in POST or GET that stores pageNumber
    *                           'action' => $action
    *                             )
    *   
    */
    public function paginate(ResultSet $rs, $paginationOptions){
        
       $paginateMetaData['rowCount'] = $rs->rowCount();                          // Number of rows in current page
       $paginateMetaData['fullRowCount'] = $rs->getFullRowCount();               // Total number of rows in database
       $paginateMetaData['pageCount'] = $rs->getPageCount();                     // Total number of pages in database
       
       $paginationOptions['page'] = empty($paginationOptions['page']) ? 1 : $paginationOptions['page']; // the page to retrieve
       $paginationOptions['uriVar'] = empty($paginationOptions['uriVar']) ? 'pageNumber' : $paginationOptions['uriVar'];
       $paginationOptions['ulClass'] = empty($paginationOptions['ulClass']) ? 'paginator' : $paginationOptions['ulClass']; // css class name for <ul>
       
       $this->paginationOptions = $paginationOptions;
       $this->paginationMetaData = $paginateMetaData;
    }
    
    /**
     * Returns rowCount, fullRowCount or pageCount
     * This is data retrieved by database
     * 
     * @param type $key
     * @return type
     */
    public function getPaginationMetaData($key){
        
        return @$this->paginationMetaData[$key];
    }
    
    /**
     * Returns page, uriVar, order, direction, numItems, action or ulClass
     * This data was set by a developer, it's not calculated data. 
     * 
     * @param type $key
     * @return type
     */
    public function getPaginationOption($key){
        
        $option = @$this->paginationOptions[$key];
        return (!empty( $option )) ? $option : @$this->paginationMetaData[$key];
    }
    
    /* Deprecado. Se mantiene por compatibilidad. Usar getPaginationOption() */
    public function getPaginateOption($opt){
        return $this->getPaginationOption($opt);
    }
    
    public function getScroller(){
        
        $numItems = ($this->getPaginateOption('numItems')) ? $this->getPaginateOption('numItems') : 3; // Number of pages to retrieve in <ul>
        
        $order = $direction = null;
        if($this->getPaginateOption('order')){
            
            $order = $this->getPaginateOption('order');
            $direction = $this->getPaginateOption('direction');
        } 
 
        $scroller = PageScroller::createScroller($this->getPaginateOption('pageCount'),// page count  
                $this->getPaginateOption('page'),// num page   
                $this->getPaginateOption('action').'?order='.$order.'&direction='.$direction.'&'.$this->getPaginateOption('uriVar').'={%PAGE}',// action
                $numItems,// items in paginator
                $this->getPaginateOption('ulClass'),// ul class name
                true 
       ); 
       return $scroller;
    }
    
    /* Bulk action */
    
    function setMainField($fieldName){
        
        $this->mainField = $fieldName;
    }
    
    function getMainField(){
    
        return $this->mainField;
    }
    
    function save(){
        
        $result = array();
        if(!empty($this->items)){
            foreach ($this->items as $item) 
            $result[] = $item->save();
        }
        return $result;
    }
        
    /**
     * This assummes each field has a PK. I will not work on tables lacking of pk.
     * @return type 
     * 
     * @version beta 1 by Galo
     */
    function delete( /* */ ){
        
        try {
            
            $table = $pk = null;
            
            foreach ($this->items as $i => $item) {

                if($i == 0){
                    
                    $table = $item->getTableName();
                    $pk = $item->getPkName();
                }
                
                $ids[] = $item->getPK();
            }
            
            $sql = " DELETE FROM $table WHERE $pk IN (".implode(", ", $ids).") ";
            $result = $item->setQuery($sql)->execute();

            return $result;
            
        } catch (Exception $exc) {
            
            echo $exc->getTraceAsString();
        }

//        if(!$this->isEmpty()){
//            foreach ($this->items as $item) 
//                $item->delete();
//        }
        
        return $this->clear();
    }       
    
    /**
     * @version beta 1 by Galo
     */
    protected function clear(){
        
        $this->items = array();
        
        return $this;
    }


    /**
     * Returns the same collection with a tree incorporated in "children" var
     * 
     * @param type $item_id
     * @return array
     */
    public function generateTree($param){
        
         if(!$this->isEmpty()){

             if(is_array($param)){
                 
                 $parentKeyName = current($param);
                 $tableName = key($param); 
             } else {
                 $parentKeyName = $param;
                 $tableName = null;
             }
             
            foreach($this->items as &$obj){

                $this->setChildren( $obj, $parentKeyName, $tableName );      
            }
         }
      
         return $this;
     }
     
    /**
     * Returns the same collection with categories tree incorporated in "parents" var
     * 
     * @param type $item_id
     * @return array
     * 
     * @version alpha 0.1 Guille
     */
    public function generateUpTree($param){
        
         if(!$this->isEmpty()){

             if(is_array($param)){
                 
                 $parentKeyName = current($param);
                 $tableName = key($param); 
             } else {
                 $parentKeyName = $param;
                 $tableName = null;
             }
             
            foreach($this->items as &$obj){

                $this->setParents( $obj, $parentKeyName, $tableName );      
            }
         }
      
         return $this;
     }
       
     /**
      * 
      * @param BaseSQLModel $obj
      * @param type $parentKeyName
      * @param type $tableName
      * 
      * @version alpha 0.1 Guille
      */
    private function setParents(BaseSQLModel &$obj, $parentKeyName, $tableName = null){
               
        try {
               
            if(gettype($obj) == 'object'){
                
                $table = (!empty($tableName)) ? $tableName : $obj->getTableName();

                $model = new BaseSQLModel( $table );

                $parents = $model
                            ->filterBy( $parentKeyName, $obj->getField( $obj->getPkName() ))
                            ->find(); // find where parent_id = current object id
                  
                $obj->setParents($parents); // magic method from Result model
                 
                if( !$parents->isEmpty() ){
                    
                    foreach($parents->items as &$c){
                     
                         $this->setParents($c, $parentKeyName); // recursive call
                    }
                }
            }
            
        } catch (Exception $exc) {
             throw $exc;
            //echo $exc->getMessage();
        }
    } 
     
    private function setChildren(BaseSQLModel &$obj, $parentKeyName, $tableName = null){
                   
        
        try {
               
            if(gettype($obj) == 'object'){
                
                $table = (!empty($tableName)) ? $tableName : $obj->getTableName();

                if(class_exists($table))
                    $model = new $table();
                else
                    $model = new BaseSQLModel( $table );

                $children = $model
                            ->filterBy( $parentKeyName, $obj->getField( $obj->getPkName() ))
                            ->find(); // find where parent_id = current object id
                  
                $obj->setChildren($children); // magic method from Result model
               
                if( !$children->isEmpty() ){
                    
                    foreach($children->items as &$c){
                     
                         $this->setChildren($c, $parentKeyName); // recursive call
                    }
                }
            }
            
        } catch (Exception $exc) {
            throw $exc;
        }
    }
}

?>
