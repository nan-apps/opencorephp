<?php
/**
 * ZedPlan OpenCorePHP Framework
 *
 * Copyright (c) 2005-2010, ZedPlan (http://www.zedplan.com)
 *
 *
 *
 * LICENSE
 *
 * This source file is subject to the GPL license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opencorephp.zedplan.com/license.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to opencorephp@zedplan.com so we can send you a copy immediately.
 *
 * @copyright	Copyright (c) 2005-2014, ZedPlan (http://www.zedplan.com)
 * @link	http://opencorephp.zedplan.com
 * @license	http://opencorephp.zedplan.com/license.txt     GPL License
 */

abstract class CLIController {

	/**
	 * @var Config
	 */
	protected $config;

    /**
	 * @var string
	 */
	public $defaultAction;

    /**
     * @var array
     */
    public $options;

    /**
     * Constructor
     *
     * @param string $defaultAction
     * @throws InvalidArgumentException if $defaultAction is invalid
     */

    public function __construct($options, $defaultAction = 'default') {
        
        $this->options = $options;

		$actionMethod = $defaultAction . 'Action';
		if (!method_exists($this, $actionMethod)) {
			throw new InvalidArgumentException("Invalid default action. Method ".get_class($this)."::{$actionMethod} does not exist.");
		}
		$this->defaultAction = $defaultAction;
		$this->config = Config::getInstance();
    }

    public function defaultAction () {
        $methods = get_class_methods($this);
    
        $actions = array_filter($methods, function ($method) {
            if ($method == $this->defaultAction."Action") return false;
            return preg_match('#^.*Action$#', $method);            
        });


        $this->writeErrorLn("Default action not defined, try with:");

        foreach ($actions as  $action) {
            $caction = preg_replace('#Action$#', '', $action);
            $this->writeErrorLn("\t --task $caction");
        }

        return 1;

    }

    /**
     * Write a line on stdout
     *
     * @param string    $msg
     */

    public function writeLn () {

        $msg = func_get_arg(0);
        $msg .="\n";
    
        fwrite(STDOUT, $msg);

    }

    /**
     * Write a line on stderr
     *
     * @param string    $msg
     */

    public function writeErrorLn () {
        
        $msg = func_get_arg(0);
        $msg .= "\n";

        fwrite(STDERR, $msg);

    }

}
